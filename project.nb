(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 6.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[     12634,        424]
NotebookOptionsPosition[     11246,        371]
NotebookOutlinePosition[     11606,        387]
CellTagsIndexPosition[     11563,        384]
WindowFrame->Normal
ContainsDynamic->False*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
   "This", " ", "is", " ", "function", " ", "is", " ", "used", " ", "to", " ",
     "calculate", " ", "the", " ", "projection", " ", "matrix"}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
   "Parameter", " ", "X", " ", "is", " ", "the", " ", "design", " ", 
    "matrix"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
   "Be", " ", "careful", " ", "with", " ", "the", " ", "form", " ", "of", " ",
     "the", " ", "design", " ", "matrix", " ", "when", " ", "you", " ", "use",
     " ", "this", " ", 
    RowBox[{"function", "!"}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Project", "[", "X_", "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"Return", "[", 
      RowBox[{"X", ".", 
       RowBox[{"PseudoInverse", "[", 
        RowBox[{
         RowBox[{"Transpose", "[", "X", "]"}], ".", "X"}], "]"}], ".", 
       RowBox[{"Transpose", "[", "X", "]"}]}], "]"}]}], "\[IndentingNewLine]",
     "]"}]}]}]], "Input",
 CellChangeTimes->{{3.4444980947905874`*^9, 3.444498222067588*^9}, {
  3.4444983348945875`*^9, 3.4444983781365876`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"x", "=", 
  RowBox[{"{", 
   RowBox[{"3", ",", "4", ",", "5", ",", "6"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.4444984069905877`*^9, 3.4444984254485874`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"3", ",", "4", ",", "5", ",", "6"}], "}"}]], "Output",
 CellChangeTimes->{3.444498428454588*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"x", "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.4444984291755877`*^9, 3.4444984378785877`*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", 
   TagBox[GridBox[{
      {"3"},
      {"4"},
      {"5"},
      {"6"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
       "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.5599999999999999]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Column], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.4444984384445877`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Project", "[", 
  RowBox[{"x", "//", "MatrixForm"}], "]"}]], "Input",
 CellChangeTimes->{{3.4444985424595876`*^9, 3.444498558876588*^9}}],

Cell[BoxData[
 RowBox[{
  TagBox[
   RowBox[{"(", "\[NoBreak]", 
    TagBox[GridBox[{
       {"3"},
       {"4"},
       {"5"},
       {"6"}
      },
      GridBoxAlignment->{
       "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
        "RowsIndexed" -> {}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.5599999999999999]}, 
          Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}, "RowsIndexed" -> {}}],
     Column], "\[NoBreak]", ")"}],
   Function[BoxForm`e$, 
    MatrixForm[BoxForm`e$]]], ".", 
  RowBox[{"PseudoInverse", "[", 
   RowBox[{
    RowBox[{"Transpose", "[", 
     TagBox[
      RowBox[{"(", "\[NoBreak]", 
       TagBox[GridBox[{
          {"3"},
          {"4"},
          {"5"},
          {"6"}
         },
         GridBoxAlignment->{
          "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, 
           "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
         GridBoxSpacings->{"Columns" -> {
             Offset[0.27999999999999997`], {
              Offset[0.5599999999999999]}, 
             Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, 
           "Rows" -> {
             Offset[0.2], {
              Offset[0.4]}, 
             Offset[0.2]}, "RowsIndexed" -> {}}],
        Column], "\[NoBreak]", ")"}],
      Function[BoxForm`e$, 
       MatrixForm[BoxForm`e$]]], "]"}], ".", 
    TagBox[
     RowBox[{"(", "\[NoBreak]", 
      TagBox[GridBox[{
         {"3"},
         {"4"},
         {"5"},
         {"6"}
        },
        GridBoxAlignment->{
         "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, 
          "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.5599999999999999]}, 
            Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, 
          "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}, "RowsIndexed" -> {}}],
       Column], "\[NoBreak]", ")"}],
     Function[BoxForm`e$, 
      MatrixForm[BoxForm`e$]]]}], "]"}], ".", 
  RowBox[{"Transpose", "[", 
   TagBox[
    RowBox[{"(", "\[NoBreak]", 
     TagBox[GridBox[{
        {"3"},
        {"4"},
        {"5"},
        {"6"}
       },
       GridBoxAlignment->{
        "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
          "RowsIndexed" -> {}},
       GridBoxSpacings->{"Columns" -> {
           Offset[0.27999999999999997`], {
            Offset[0.5599999999999999]}, 
           Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.2], {
            Offset[0.4]}, 
           Offset[0.2]}, "RowsIndexed" -> {}}],
      Column], "\[NoBreak]", ")"}],
    Function[BoxForm`e$, 
     MatrixForm[BoxForm`e$]]], "]"}]}]], "Output",
 CellChangeTimes->{{3.4444985479195876`*^9, 3.4444985596065874`*^9}}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.4444984464605875`*^9, 3.444498472890588*^9}, 
   3.4444985707825875`*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Transpose", "[", 
   RowBox[{"{", "x", "}"}], "]"}], "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.4444985118205876`*^9, 3.4444985275945873`*^9}, {
  3.4444985819295874`*^9, 3.4444986126035876`*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"3"},
     {"4"},
     {"5"},
     {"6"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.4444985204455876`*^9, 3.4444985288085876`*^9}, {
  3.4444985870065875`*^9, 3.4444986132685876`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Project", "[", 
   RowBox[{"Transpose", "[", 
    RowBox[{"{", "x", "}"}], "]"}], "]"}], "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.444498629744588*^9, 3.4444986500315876`*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      FractionBox["9", "86"], 
      FractionBox["6", "43"], 
      FractionBox["15", "86"], 
      FractionBox["9", "43"]},
     {
      FractionBox["6", "43"], 
      FractionBox["8", "43"], 
      FractionBox["10", "43"], 
      FractionBox["12", "43"]},
     {
      FractionBox["15", "86"], 
      FractionBox["10", "43"], 
      FractionBox["25", "86"], 
      FractionBox["15", "43"]},
     {
      FractionBox["9", "43"], 
      FractionBox["12", "43"], 
      FractionBox["15", "43"], 
      FractionBox["18", "43"]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.4444986383345876`*^9, 3.4444986508125877`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Transpose", "[", "x", "]"}]], "Input",
 CellChangeTimes->{{3.444498675541588*^9, 3.444498679126588*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Transpose", "::", "\<\"nmtx\"\>"}], 
  RowBox[{
  ":", " "}], "\<\"The first two levels of the one-dimensional list \\!\\({3, \
4, 5, 6}\\) cannot be transposed. \
\\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", \
ButtonFrame->None, ButtonData:>\\\"paclet:ref/message/Transpose/nmtx\\\", \
ButtonNote -> \\\"Transpose::nmtx\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{3.4444986802895875`*^9}],

Cell[BoxData[
 RowBox[{"Transpose", "[", 
  RowBox[{"{", 
   RowBox[{"3", ",", "4", ",", "5", ",", "6"}], "}"}], "]"}]], "Output",
 CellChangeTimes->{3.444498680305588*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"{", "x", "}"}], "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.4444987016095877`*^9, 3.4444987104915876`*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"3", "4", "5", "6"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.4444987112435875`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Table", "[", 
  RowBox[{"i", ",", 
   RowBox[{"{", 
    RowBox[{"i", ",", "3", ",", "6"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.4444987714675875`*^9, 3.4444987806305876`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"3", ",", "4", ",", "5", ",", "6"}], "}"}]], "Output",
 CellChangeTimes->{3.444498781376588*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Table", "[", 
  RowBox[{"j", ",", 
   RowBox[{"{", 
    RowBox[{"i", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"j", ",", "3", ",", "6"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.4444987922385874`*^9, 3.4444988230355873`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"3", ",", "4", ",", "5", ",", "6"}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.444498823994588*^9}]
}, Open  ]],

Cell[BoxData[""], "Input"]
},
WindowSize->{1264, 599},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
Magnification->2.,
FrontEndVersion->"6.0 for Microsoft Windows (32-bit) (April 28, 2007)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[568, 21, 1214, 30, 262, "Input"],
Cell[CellGroupData[{
Cell[1807, 55, 186, 4, 57, "Input"],
Cell[1996, 61, 135, 3, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2168, 69, 129, 2, 57, "Input"],
Cell[2300, 73, 695, 22, 152, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3032, 100, 161, 3, 57, "Input"],
Cell[3196, 105, 2993, 92, 152, "Output"]
}, Open  ]],
Cell[6204, 200, 122, 2, 57, "Input"],
Cell[CellGroupData[{
Cell[6351, 206, 242, 5, 57, "Input"],
Cell[6596, 213, 720, 21, 152, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7353, 239, 223, 5, 57, "Input"],
Cell[7579, 246, 1153, 36, 200, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8769, 287, 128, 2, 57, "Input"],
Cell[8900, 291, 461, 9, 46, "Message"],
Cell[9364, 302, 172, 4, 82, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9573, 311, 152, 3, 57, "Input"],
Cell[9728, 316, 620, 17, 82, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10385, 338, 210, 5, 57, "Input"],
Cell[10598, 345, 135, 3, 82, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10770, 353, 269, 7, 57, "Input"],
Cell[11042, 362, 159, 4, 82, "Output"]
}, Open  ]],
Cell[11216, 369, 26, 0, 57, "Input"]
}
]
*)

(* End of internal cache information *)
