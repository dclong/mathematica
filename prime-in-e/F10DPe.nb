(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 6.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[      9042,        293]
NotebookOptionsPosition[      7614,        237]
NotebookOutlinePosition[      7974,        253]
CellTagsIndexPosition[      7931,        250]
WindowFrame->Normal
ContainsDynamic->False*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[""], "Input",
 CellChangeTimes->{3.46584465805*^9, 3.465845950506*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.465844659942*^9, 3.4658446860559998`*^9}, {
  3.465844790104*^9, 3.4658447907790003`*^9}, {3.465845933657*^9, 
  3.4658459344230003`*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.4658447923859997`*^9, 3.465844823823*^9}, {
  3.465845939359*^9, 3.465845940123*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{3.465844837035*^9, 3.4658459442720003`*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{3.465844842035*^9, 3.465845947052*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"F10DPe", "[", 
   RowBox[{
    RowBox[{"k_:", "10"}], ",", 
    RowBox[{"n_:", "5000"}]}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"app", ",", "temp", ",", "i", ",", "flag"}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"app", "=", 
      RowBox[{"N", "[", 
       RowBox[{"E", ",", "n"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"flag", "=", "0"}], ";", "\[IndentingNewLine]", 
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"i", "=", "1"}], ",", 
       RowBox[{"i", "\[LessEqual]", 
        RowBox[{"n", "-", "k"}]}], ",", 
       RowBox[{"i", "++"}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"temp", "=", 
         RowBox[{"app", "*", 
          RowBox[{"10", "^", 
           RowBox[{"(", 
            RowBox[{"k", "-", "1"}], ")"}]}]}]}], ";", "\[IndentingNewLine]", 
        
        RowBox[{"temp", "=", 
         RowBox[{"Floor", "[", "temp", "]"}]}], ";", "\[IndentingNewLine]", 
        RowBox[{"flag", "++"}], ";", "\[IndentingNewLine]", 
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{"PrimeQ", "[", "temp", "]"}], ",", 
          RowBox[{
           RowBox[{"Print", "[", 
            RowBox[{
            "\"\<Congratulations! You have found the first \>\"", ",", "k", 
             ",", "\"\< digists prime:\>\"", ",", "temp"}], "]"}], ";", 
           RowBox[{"Print", "[", 
            RowBox[{"\"\<The starting position is:\>\"", " ", ",", "flag"}], 
            "]"}], ";", 
           RowBox[{"Break", "[", "]"}]}], ",", 
          RowBox[{"app", "=", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{"app", "-", 
              RowBox[{"Floor", "[", "app", "]"}]}], ")"}], " ", "10"}]}]}], 
         "]"}]}]}], "\[IndentingNewLine]", "]"}]}]}], "\[IndentingNewLine]", 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.4658448763570004`*^9, 3.465844924457*^9}, {
   3.465844989542*^9, 3.465845003116*^9}, {3.4658450407460003`*^9, 
   3.4658450927860003`*^9}, {3.4658451484630003`*^9, 3.465845229758*^9}, {
   3.465845281109*^9, 3.465845487422*^9}, {3.465845520174*^9, 
   3.465845525349*^9}, {3.465845590916*^9, 3.4658456994189997`*^9}, 
   3.4658457296549997`*^9, {3.4658458272790003`*^9, 3.465845828138*^9}, {
   3.4658458865030003`*^9, 3.4658458874379997`*^9}, {3.4658461208389997`*^9, 
   3.465846124133*^9}, {3.4659254478162003`*^9, 3.4659254845552*^9}, {
   3.4659256507181997`*^9, 3.4659256795522003`*^9}, {3.4659257539042*^9, 
   3.4659257857172003`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"F10DPe", "[", "]"}]], "Input",
 CellChangeTimes->{{3.4658455383719997`*^9, 3.465845542432*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Congratulations! You have found the 10 digists prime:\"\>", 
   "\[InvisibleSpace]", "7427466391"}],
  SequenceForm[
  "Congratulations! You have found the 10 digists prime:", 7427466391],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.465845543698*^9, 3.4658457089189997`*^9, 3.465845783064*^9, {
   3.465845836159*^9, 3.465845839149*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"The starting position:\"\>", "\[InvisibleSpace]", "100"}],
  SequenceForm["The starting position:", 100],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.465845543698*^9, 3.4658457089189997`*^9, 3.465845783064*^9, {
   3.465845836159*^9, 3.465845839153*^9}}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[InvisibleSpace]", 
  RowBox[{"PrimeQ", "[", "7427466391", "]"}]}]], "Input",
 CellChangeTimes->{{3.465845579007*^9, 3.4658455834379997`*^9}}],

Cell[BoxData["True"], "Output",
 CellChangeTimes->{3.465845584328*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Floor", "[", 
  RowBox[{"a", "*", 
   RowBox[{"10", "^", "9"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.465845803534*^9, 3.465845810608*^9}}],

Cell[BoxData["2718281828"], "Output",
 CellChangeTimes->{3.46584581184*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", 
  RowBox[{"E", ",", "110"}], "]"}]], "Input",
 CellChangeTimes->{{3.465845848082*^9, 3.4658458605690002`*^9}, {
  3.465845895467*^9, 3.465845896894*^9}}],

Cell[BoxData["2.\
718281828459045235360287471352662497757247093699959574966967627724076630353547\
59457138217852516642742746639193200305992181742`110."], "Output",
 CellChangeTimes->{3.465845861297*^9, 3.465845897612*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"F10DPe", "[", "]"}]], "Input",
 CellChangeTimes->{{3.4659257157102003`*^9, 3.4659257244802*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Congratulations! You have found the first \"\>", 
   "\[InvisibleSpace]", "10", "\[InvisibleSpace]", "\<\"digists prime:\"\>", 
   "\[InvisibleSpace]", "7427466391"}],
  SequenceForm[
  "Congratulations! You have found the first ", 10, "digists prime:", 
   7427466391],
  Editable->False]], "Print",
 CellChangeTimes->{3.4659257370411997`*^9}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"The starting position is:\"\>", "\[InvisibleSpace]", "100"}],
  SequenceForm["The starting position is:", 100],
  Editable->False]], "Print",
 CellChangeTimes->{3.4659257370502*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"F10DPe", "[", "11", "]"}]], "Input",
 CellChangeTimes->{{3.4659257667072*^9, 3.4659257723202*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Congratulations! You have found the first \"\>", 
   "\[InvisibleSpace]", "11", "\[InvisibleSpace]", "\<\"digists prime:\"\>", 
   "\[InvisibleSpace]", "75724709369"}],
  SequenceForm[
  "Congratulations! You have found the first ", 11, "digists prime:", 
   75724709369],
  Editable->False]], "Print",
 CellChangeTimes->{3.4659257731042*^9}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"The starting position is:\"\>", "\[InvisibleSpace]", "38"}],
  SequenceForm["The starting position is:", 38],
  Editable->False]], "Print",
 CellChangeTimes->{3.4659257731042*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", 
  RowBox[{"E", ",", "55"}], "]"}]], "Input",
 CellChangeTimes->{{3.4659258015572*^9, 3.4659258234972*^9}}],

Cell[BoxData["2.\
7182818284590452353602874713526624977572470936999595749669676277241`55."], \
"Output",
 CellChangeTimes->{{3.4659258125832*^9, 3.4659258243302*^9}}]
}, Open  ]]
},
WindowSize->{1264, 599},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
Magnification->2.,
FrontEndVersion->"6.0 for Microsoft Windows (32-bit) (April 28, 2007)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[568, 21, 83, 1, 57, "Input"],
Cell[654, 24, 187, 3, 57, "Input"],
Cell[844, 29, 134, 2, 57, "Input"],
Cell[981, 33, 89, 1, 57, "Input"],
Cell[1073, 36, 84, 1, 57, "Input"],
Cell[1160, 39, 2555, 60, 432, "Input"],
Cell[CellGroupData[{
Cell[3740, 103, 119, 2, 57, "Input"],
Cell[CellGroupData[{
Cell[3884, 109, 396, 9, 43, "Print"],
Cell[4283, 120, 313, 7, 43, "Print"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[4645, 133, 167, 3, 57, "Input"],
Cell[4815, 138, 70, 1, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4922, 144, 166, 4, 57, "Input"],
Cell[5091, 150, 75, 1, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5203, 156, 187, 4, 57, "Input"],
Cell[5393, 162, 221, 3, 92, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5651, 170, 120, 2, 57, "Input"],
Cell[CellGroupData[{
Cell[5796, 176, 393, 9, 43, "Print"],
Cell[6192, 187, 230, 5, 43, "Print"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6471, 198, 122, 2, 57, "Input"],
Cell[CellGroupData[{
Cell[6618, 204, 391, 9, 43, "Print"],
Cell[7012, 215, 228, 5, 43, "Print"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7289, 226, 140, 3, 57, "Input"],
Cell[7432, 231, 166, 3, 57, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
