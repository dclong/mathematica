(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 8.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc.                                               *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       835,         17]
NotebookDataLength[      3652,        102]
NotebookOptionsPosition[      4094,         99]
NotebookOutlinePosition[      4430,        114]
CellTagsIndexPosition[      4387,        111]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "Consider", " ", "that", " ", "two", " ", "people", " ", "toss", " ", "a",
      " ", "coin", " ", "for", " ", "m", " ", "and", " ", "n", " ", "times", 
     " ", "respectively"}], ",", " ", "\n", 
    RowBox[{
    "this", " ", "function", " ", "caculate", " ", "the", " ", "probability", 
     " ", "that", " ", "the", " ", "second", " ", "people", " ", "have", " ", 
     "x", " ", "more", " ", "heads", " ", "than", " ", "the", " ", "first", 
     " ", 
     RowBox[{"peoople", ".", "\n", "Parameter"}], " ", "m", " ", "is", " ", 
     "the", " ", "times", " ", "the", " ", "first", " ", "people", " ", 
     "tosses", " ", "the", " ", 
     RowBox[{"coin", ".", "\n", "Parameter"}], " ", "n", " ", "is", " ", 
     "the", " ", "times", " ", "the", " ", "second", " ", "people", " ", 
     "tosses", " ", "the", " ", 
     RowBox[{"coin", ".", "\n", "Parameter"}], " ", "prob", " ", "is", " ", 
     "the", " ", "probability", " ", "that", " ", "the", " ", "heads", " ", 
     "come", " ", 
     RowBox[{"up", ".", "\n", "Parameter"}], " ", "DiffMoreThan", " ", 
     "specifies", " ", "at", " ", "least", " ", "how", " ", "many", " ", 
     "more", " ", "heads", " ", "the", " ", "second", " ", "people", " ", 
     "get", " ", "than", " ", "the", " ", "first", " ", "people"}]}], " ", 
   "*)"}], "\n", 
  RowBox[{
   RowBox[{"ProbTwoComp", "[", 
    RowBox[{
     RowBox[{"m_:", "Integer"}], ",", 
     RowBox[{"n_:", "Integer"}], ",", 
     RowBox[{"DiffMoreThan_:", "0"}], ",", 
     RowBox[{"prob_:", 
      RowBox[{"1", "/", "2"}]}]}], "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"i", ",", "j"}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Return", "[", 
       RowBox[{"Sum", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"Binomial", "[", 
           RowBox[{"m", ",", "i"}], "]"}], " ", 
          RowBox[{"prob", "^", "i"}], " ", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"1", "-", "prob"}], ")"}], "^", 
           RowBox[{"(", 
            RowBox[{"m", "-", "i"}], ")"}]}], " ", 
          RowBox[{"Binomial", "[", 
           RowBox[{"n", ",", "j"}], "]"}], " ", 
          RowBox[{"prob", "^", "j"}], " ", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"1", "-", "prob"}], ")"}], "^", 
           RowBox[{"(", 
            RowBox[{"n", "-", "j"}], ")"}]}]}], ",", 
         RowBox[{"{", 
          RowBox[{"i", ",", "0", ",", "m"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"j", ",", 
           RowBox[{"i", "+", "DiffMoreThan", "+", "1"}], ",", "n"}], "}"}]}], 
        "]"}], "]"}], ";"}]}], "\n", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.433775440201346*^9, 3.433775471311346*^9}, {
  3.433775510726346*^9, 3.433775589916346*^9}}]
},
WindowSize->{607, 549},
WindowMargins->{{54, Automatic}, {48, Automatic}},
FrontEndVersion->"8.0 for Linux x86 (64-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[1235, 30, 2855, 67, 297, "Input"]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature 8wT1Kt4#7nk9OBKG7F6P3WMv *)
