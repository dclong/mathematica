(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 6.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[      5494,        174]
NotebookOptionsPosition[      4575,        140]
NotebookOutlinePosition[      4916,        155]
CellTagsIndexPosition[      4873,        152]
WindowFrame->Normal
ContainsDynamic->False*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{
  "This", " ", "Function", " ", "is", " ", "used", " ", "to", " ", 
   "calculate", " ", "the", " ", "expected", " ", "value", " ", "of", " ", 
   "Binomial", " ", 
   RowBox[{"distribution", ".", " ", 
    RowBox[{"It", "'"}]}], "s", " ", "of", " ", "little", " ", "use", " ", 
   "and", " ", "jsut", " ", "for", " ", "fun"}], "*)"}]], "Input",
 CellChangeTimes->{{3.4430166840150003`*^9, 3.443016714758*^9}, {
  3.443017347186*^9, 3.44301741255*^9}, {3.479422508539*^9, 
  3.479422538913*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"BinomialMoment", "[", 
    RowBox[{"0", ",", "n_", ",", "p_"}], "]"}], "=", "1"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"BinomialMoment", "[", 
    RowBox[{"k_", ",", "1", ",", "p_"}], "]"}], ":=", "p"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"BinomialMoment", "[", 
   RowBox[{"k_", ",", "n_", ",", "p_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "i", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"n", " ", "p", " ", 
     RowBox[{"Sum", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"Binomial", "[", 
         RowBox[{
          RowBox[{"k", "-", "1"}], ",", "i"}], "]"}], " ", 
        RowBox[{"BinomialMoment", "[", 
         RowBox[{"i", ",", 
          RowBox[{"n", "-", "1"}], ",", "p"}], "]"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "0", ",", 
         RowBox[{"k", "-", "1"}]}], "}"}]}], "]"}]}]}], "\[IndentingNewLine]",
    "]"}]}]}], "Input",
 CellChangeTimes->{{3.443014848325549*^9, 3.4430149096045485`*^9}, {
   3.4430149478335485`*^9, 3.443014989368549*^9}, {3.4430150229825487`*^9, 
   3.443015030086549*^9}, {3.4430150664445486`*^9, 3.443015078954549*^9}, {
   3.4430157036470003`*^9, 3.443015703858*^9}, {3.4430157431099997`*^9, 
   3.443015744101*^9}, {3.44301578749*^9, 3.4430159253310003`*^9}, 
   3.443017100835*^9, {3.4430175785690002`*^9, 3.443017624587*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"BinomialMoment", "[", 
  RowBox[{"1", ",", "10", ",", "0"}], "]"}]], "Input",
 CellChangeTimes->{{3.443015950902*^9, 3.443015962342*^9}, {
  3.4430176028059998`*^9, 3.443017604094*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.443015963365*^9, 3.4430172859379997`*^9, 
  3.443017604892*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"BinomialMoment", "[", 
  RowBox[{"10", ",", "2", ",", "0.5"}], "]"}]], "Input",
 CellChangeTimes->{{3.443016249218*^9, 3.443016254443*^9}, {3.443017611576*^9,
   3.443017615425*^9}}],

Cell[BoxData["256.5`"], "Output",
 CellChangeTimes->{
  3.443016256204*^9, 3.443017288525*^9, {3.443017616053*^9, 
   3.4430176365620003`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"BinomialMoment", "[", 
  RowBox[{"1", ",", "10", ",", "0.5"}], "]"}]], "Input",
 CellChangeTimes->{{3.4430159774379997`*^9, 3.443015977584*^9}, {
  3.443017640954*^9, 3.443017642283*^9}}],

Cell[BoxData["5.`"], "Output",
 CellChangeTimes->{3.443015979492*^9, 3.443017290973*^9, 3.443017643587*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.443015995836*^9, 3.44301599595*^9}, 3.4430174471*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.443016032309*^9, 3.443016043793*^9}, 3.443017443436*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{3.443016023337*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.443014994402549*^9, 3.443015018717549*^9}, 
   3.443017440205*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{
  3.4430150902655487`*^9, {3.443015757074*^9, 3.443015757177*^9}, 
   3.443017436749*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.4430151140485487`*^9, 3.443015116952549*^9}, {
   3.443015766646*^9, 3.443015766777*^9}, 3.44301743315*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.443015665363*^9, 3.443015670991*^9}, 3.443017429549*^9}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.443015933984*^9, 3.443015939159*^9}, 
   3.4430174252790003`*^9}]
},
WindowSize->{1264, 679},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"6.0 for Microsoft Windows (32-bit) (April 28, 2007)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[568, 21, 536, 11, 31, "Input"],
Cell[1107, 34, 1433, 36, 112, "Input"],
Cell[CellGroupData[{
Cell[2565, 74, 209, 4, 31, "Input"],
Cell[2777, 80, 113, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2927, 87, 206, 4, 31, "Input"],
Cell[3136, 93, 143, 3, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3316, 101, 211, 4, 31, "Input"],
Cell[3530, 107, 107, 1, 30, "Output"]
}, Open  ]],
Cell[3652, 111, 102, 1, 31, "Input"],
Cell[3757, 114, 105, 1, 31, "Input"],
Cell[3865, 117, 65, 1, 31, "Input"],
Cell[3933, 120, 115, 2, 31, "Input"],
Cell[4051, 124, 136, 3, 31, "Input"],
Cell[4190, 129, 156, 2, 31, "Input"],
Cell[4349, 133, 105, 1, 31, "Input"],
Cell[4457, 136, 114, 2, 31, "Input"]
}
]
*)

(* End of internal cache information *)
