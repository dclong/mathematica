(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 8.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc.                                               *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       835,         17]
NotebookDataLength[      2829,         93]
NotebookOptionsPosition[      3198,         87]
NotebookOutlinePosition[      3534,        102]
CellTagsIndexPosition[      3491,         99]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"Graphics3D", "[", 
  RowBox[{
   RowBox[{"{", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"Yellow", ",", 
       RowBox[{"Opacity", "[", "0.9", "]"}], ",", 
       RowBox[{"Cuboid", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"1", ",", "1", ",", "1.7"}], "}"}]}], "]"}]}], "}"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Arrow", "[", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"1.5", ",", "0", ",", "0"}], "}"}]}], "}"}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Arrow", "[", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "1.5", ",", "0"}], "}"}]}], "}"}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Arrow", "[", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "0"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "0", ",", "2"}], "}"}]}], "}"}], "]"}]}], 
    "\[IndentingNewLine]", "}"}], ",", "\[IndentingNewLine]", 
   RowBox[{"Boxed", "\[Rule]", "False"}]}], "\[IndentingNewLine]", 
  "]"}]], "Input"],

Cell[BoxData[
 Graphics3DBox[{
   {RGBColor[1, 1, 0], Opacity[0.9], CuboidBox[{0, 0, 0}, {1, 1, 1.7}]}, 
   Arrow3DBox[{{0, 0, 0}, {1.5, 0, 0}}], Arrow3DBox[{{0, 0, 0}, {0, 1.5, 0}}],
    Arrow3DBox[{{0, 0, 0}, {0, 0, 2}}]},
  Boxed->False,
  ImageSize->{194.52157313448808`, 251.51515151515153`},
  ViewPoint->{2.787712083586503, 1.1323549707695142`, 1.5480418467210006`},
  ViewVertical->{0.18310098745553513`, 0.10431377193818778`, 
   0.987431452949758}]], "Output",
 CellChangeTimes->{3.525105614275463*^9, 3.551009418095208*^9}]
}, Open  ]]
},
WindowSize->{1350, 633},
WindowMargins->{{Automatic, 5}, {Automatic, -8}},
FrontEndVersion->"8.0 for Linux x86 (64-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1257, 32, 1388, 40, 164, "Input"],
Cell[2648, 74, 534, 10, 267, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature sxDY8JjUHumKdD16HGScK@ml *)
