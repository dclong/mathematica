(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 6.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[      6157,        246]
NotebookOptionsPosition[      4831,        193]
NotebookOutlinePosition[      5193,        209]
CellTagsIndexPosition[      5150,        206]
WindowFrame->Normal
ContainsDynamic->False*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"x0", "=", 
  RowBox[{"Table", "[", 
   RowBox[{"1", ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "8"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.451071574592347*^9, 3.4510715937743473`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1"}],
   "}"}]], "Output",
 CellChangeTimes->{3.451071595770347*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"x1", "=", 
  RowBox[{"{", 
   RowBox[{
   "1", ",", "1", ",", "3", ",", "3", ",", "3", ",", "4", ",", "4", ",", 
    "4"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.4510716019323473`*^9, 3.4510716197913475`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "1", ",", "1", ",", "3", ",", "3", ",", "3", ",", "4", ",", "4", ",", "4"}],
   "}"}]], "Output",
 CellChangeTimes->{3.4510716208663473`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"x1", ".", "x0"}], "/", 
  RowBox[{"x0", ".", "x0"}]}]], "Input",
 CellChangeTimes->{{3.4510716751953473`*^9, 3.4510716849193473`*^9}}],

Cell[BoxData[
 FractionBox["23", "8"]], "Output",
 CellChangeTimes->{3.451071686514347*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"z1", "=", 
  RowBox[{"x1", "-", 
   RowBox[{
    RowBox[{
     RowBox[{"x1", ".", "x0"}], "/", 
     RowBox[{"(", 
      RowBox[{"x0", ".", "x0"}], ")"}]}], " ", "x0"}]}]}]], "Input",
 CellChangeTimes->{{3.451071698502347*^9, 3.4510717339453473`*^9}, {
  3.4510718123053474`*^9, 3.4510718166633472`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", 
    FractionBox["15", "8"]}], ",", 
   RowBox[{"-", 
    FractionBox["15", "8"]}], ",", 
   FractionBox["1", "8"], ",", 
   FractionBox["1", "8"], ",", 
   FractionBox["1", "8"], ",", 
   FractionBox["9", "8"], ",", 
   FractionBox["9", "8"], ",", 
   FractionBox["9", "8"]}], "}"}]], "Output",
 CellChangeTimes->{{3.4510717019453473`*^9, 3.451071735027347*^9}, 
   3.451071817263347*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"z0", "=", "x0"}]], "Input",
 CellChangeTimes->{{3.4510718056093473`*^9, 3.4510718238173475`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1"}],
   "}"}]], "Output",
 CellChangeTimes->{3.451071824816347*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"t", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"-", "0.568574"}], ",", "0.0379049", ",", "0.3411441"}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.451071832151347*^9, 3.451071857462347*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "0.568574`"}], ",", "0.0379049`", ",", "0.3411441`"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.451071858637347*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"coef2", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "15"}], "/", "4"}], ",", 
    RowBox[{"3", "/", "8"}], ",", 
    RowBox[{"27", "/", "8"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.4510718727933474`*^9, 3.451071886582347*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", 
    FractionBox["15", "4"]}], ",", 
   FractionBox["3", "8"], ",", 
   FractionBox["27", "8"]}], "}"}]], "Output",
 CellChangeTimes->{3.451071895256347*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"coef2", "/", "t"}]], "Input",
 CellChangeTimes->{{3.451071899711347*^9, 3.4510719015313473`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "6.595447558277375`", ",", "9.893180037409412`", ",", "9.89318003740941`"}],
   "}"}]], "Output",
 CellChangeTimes->{3.4510719023073473`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"t", "/", "coef2"}]], "Input",
 CellChangeTimes->{{3.451071907822347*^9, 3.451071911770347*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0.15161973333333334`", ",", "0.10107973333333332`", ",", 
   "0.10107973333333332`"}], "}"}]], "Output",
 CellChangeTimes->{3.4510719123313475`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"23", "/", "8"}]], "Input",
 CellChangeTimes->{{3.451071978526347*^9, 3.451071982837347*^9}}],

Cell[BoxData[
 FractionBox["23", "8"]], "Output",
 CellChangeTimes->{3.451071984134347*^9}]
}, Open  ]]
},
WindowSize->{586, 438},
WindowMargins->{{244, Automatic}, {48, Automatic}},
Magnification->2.,
FrontEndVersion->"6.0 for Microsoft Windows (32-bit) (April 28, 2007)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[590, 23, 225, 6, 57, "Input"],
Cell[818, 31, 181, 5, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1036, 41, 236, 6, 57, "Input"],
Cell[1275, 49, 183, 5, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1495, 59, 169, 4, 57, "Input"],
Cell[1667, 65, 91, 2, 78, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1795, 72, 328, 9, 57, "Input"],
Cell[2126, 83, 445, 14, 135, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2608, 102, 121, 2, 57, "Input"],
Cell[2732, 106, 181, 5, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2950, 116, 219, 6, 92, "Input"],
Cell[3172, 124, 173, 5, 92, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3382, 134, 276, 8, 57, "Input"],
Cell[3661, 144, 213, 7, 103, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3911, 156, 121, 2, 57, "Input"],
Cell[4035, 160, 183, 5, 82, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4255, 170, 119, 2, 57, "Input"],
Cell[4377, 174, 191, 5, 82, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4605, 184, 116, 2, 57, "Input"],
Cell[4724, 188, 91, 2, 103, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

