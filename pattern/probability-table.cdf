(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 8.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc.                                               *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       835,         17]
NotebookDataLength[     11997,        384]
NotebookOptionsPosition[     11863,        362]
NotebookOutlinePosition[     12322,        380]
CellTagsIndexPosition[     12279,        377]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"patterns", "=", 
   RowBox[{"Tuples", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\"\<T\>\"", ",", "\"\<H\>\""}], "}"}], ",", "3"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ProbMatrix", "=", 
   RowBox[{"Table", "[", 
    RowBox[{"Null", ",", 
     RowBox[{"{", "8", "}"}], ",", 
     RowBox[{"{", "8", "}"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"For", "[", 
   RowBox[{
    RowBox[{"i", "=", "1"}], ",", 
    RowBox[{"i", "<", "8"}], ",", 
    RowBox[{"i", "++"}], ",", "\[IndentingNewLine]", 
    RowBox[{"For", "[", 
     RowBox[{
      RowBox[{"j", "=", 
       RowBox[{"i", "+", "1"}]}], ",", 
      RowBox[{"j", "\[LessEqual]", "8"}], ",", 
      RowBox[{"j", "++"}], ",", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"probs", "=", 
        RowBox[{"PatternFirstComeOutProbability", "[", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{
            RowBox[{"patterns", "[", 
             RowBox[{"[", "i", "]"}], "]"}], ",", 
            RowBox[{"patterns", "[", 
             RowBox[{"[", "j", "]"}], "]"}]}], "}"}], ",", 
          RowBox[{"{", 
           RowBox[{"\"\<H\>\"", ",", "\"\<T\>\""}], "}"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"7", "/", "10"}], ",", 
            RowBox[{"3", "/", "10"}]}], "}"}]}], "]"}]}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"ProbMatrix", "[", 
         RowBox[{"[", 
          RowBox[{"i", ",", "j"}], "]"}], "]"}], "=", 
        RowBox[{"probs", "[", 
         RowBox[{"[", "2", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"ProbMatrix", "[", 
         RowBox[{"[", 
          RowBox[{"j", ",", "i"}], "]"}], "]"}], "=", 
        RowBox[{"probs", "[", 
         RowBox[{"[", "3", "]"}], "]"}]}], ";"}]}], "\[IndentingNewLine]", 
     "]"}]}], "\[IndentingNewLine]", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"For", "[", 
   RowBox[{
    RowBox[{"i", "=", "1"}], ",", 
    RowBox[{"i", "\[LessEqual]", "8"}], ",", 
    RowBox[{"i", "++"}], ",", "\[IndentingNewLine]", 
    RowBox[{"AppendTo", "[", 
     RowBox[{
      RowBox[{"ProbMatrix", "[", 
       RowBox[{"[", "i", "]"}], "]"}], ",", 
      RowBox[{"Min", "[", 
       RowBox[{"Drop", "[", 
        RowBox[{
         RowBox[{"ProbMatrix", "[", 
          RowBox[{"[", "i", "]"}], "]"}], ",", 
         RowBox[{"Flatten", "[", 
          RowBox[{"Position", "[", 
           RowBox[{
            RowBox[{"ProbMatrix", "[", 
             RowBox[{"[", "i", "]"}], "]"}], ",", "Null"}], "]"}], "]"}]}], 
        "]"}], "]"}]}], "]"}]}], "\[IndentingNewLine]", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"names", "=", 
   RowBox[{"StringJoin", "/@", "patterns"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"TableForm", "[", 
  RowBox[{"ProbMatrix", ",", 
   RowBox[{"TableHeadings", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"AppendTo", "[", 
       RowBox[{"names", ",", "\"\<Min\>\""}], "]"}], ",", "names"}], 
     "}"}]}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"patterns", "[", 
  RowBox[{"[", 
   RowBox[{"Flatten", "[", 
    RowBox[{"Position", "[", 
     RowBox[{
      RowBox[{"ProbMatrix", "[", 
       RowBox[{"[", 
        RowBox[{"All", ",", "9"}], "]"}], "]"}], ",", 
      RowBox[{"Max", "[", 
       RowBox[{"ProbMatrix", "[", 
        RowBox[{"[", 
         RowBox[{"All", ",", "9"}], "]"}], "]"}], "]"}]}], "]"}], "]"}], 
   "]"}], "]"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.5042672387607327`*^9, 3.504267245032091*^9}, {
  3.5042672876045265`*^9, 3.504267289915659*^9}, {3.504402546380056*^9, 
  3.504402600911175*^9}, {3.5044026411534767`*^9, 3.5044026421325326`*^9}, {
  3.5044027219300966`*^9, 3.5044027777492895`*^9}, {3.5044028222538347`*^9, 
  3.5044028853444433`*^9}, {3.5044029420426865`*^9, 3.5044029509881983`*^9}, {
  3.504412430405845*^9, 3.5044124576274023`*^9}, {3.5044168344857445`*^9, 
  3.504416843532262*^9}, {3.50441728898274*^9, 3.504417310541973*^9}, {
  3.50442279420162*^9, 3.5044228416193323`*^9}}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      StyleBox["\[Null]",
       ShowStringCharacters->False], 
      TagBox["\<\"TTT\"\>",
       HoldForm], 
      TagBox["\<\"TTH\"\>",
       HoldForm], 
      TagBox["\<\"THT\"\>",
       HoldForm], 
      TagBox["\<\"THH\"\>",
       HoldForm], 
      TagBox["\<\"HTT\"\>",
       HoldForm], 
      TagBox["\<\"HTH\"\>",
       HoldForm], 
      TagBox["\<\"HHT\"\>",
       HoldForm], 
      TagBox["\<\"HHH\"\>",
       HoldForm], 
      TagBox["\<\"Min\"\>",
       HoldForm]},
     {
      TagBox["\<\"TTT\"\>",
       HoldForm], "Null", 
      FractionBox["3", "10"], 
      FractionBox["30", "121"], 
      FractionBox["90", "727"], 
      FractionBox["27", "1000"], 
      FractionBox["1089", "7900"], 
      FractionBox["459", "7270"], 
      FractionBox["5913", "53590"], 
      FractionBox["27", "1000"]},
     {
      TagBox["\<\"TTH\"\>",
       HoldForm], 
      FractionBox["7", "10"], "Null", 
      FractionBox["10", "17"], 
      FractionBox["30", "79"], 
      FractionBox["9", "100"], 
      FractionBox["363", "1000"], 
      FractionBox["153", "790"], 
      FractionBox["1971", "6430"], 
      FractionBox["9", "100"]},
     {
      TagBox["\<\"THT\"\>",
       HoldForm], 
      FractionBox["91", "121"], 
      FractionBox["7", "17"], "Null", 
      FractionBox["3", "10"], 
      FractionBox["79", "170"], 
      FractionBox["153", "790"], 
      FractionBox["153", "1000"], 
      FractionBox["1971", "7900"], 
      FractionBox["153", "1000"]},
     {
      TagBox["\<\"THH\"\>",
       HoldForm], 
      FractionBox["637", "727"], 
      FractionBox["49", "79"], 
      FractionBox["7", "10"], "Null", 
      FractionBox["7", "10"], 
      FractionBox["51", "130"], 
      FractionBox["51", "100"], 
      FractionBox["657", "1000"], 
      FractionBox["51", "130"]},
     {
      TagBox["\<\"HTT\"\>",
       HoldForm], 
      FractionBox["973", "1000"], 
      FractionBox["91", "100"], 
      FractionBox["91", "170"], 
      FractionBox["3", "10"], "Null", 
      FractionBox["3", "10"], 
      FractionBox["9", "79"], 
      FractionBox["153", "643"], 
      FractionBox["9", "79"]},
     {
      TagBox["\<\"HTH\"\>",
       HoldForm], 
      FractionBox["6811", "7900"], 
      FractionBox["637", "1000"], 
      FractionBox["637", "790"], 
      FractionBox["79", "130"], 
      FractionBox["7", "10"], "Null", 
      FractionBox["3", "13"], 
      FractionBox["51", "121"], 
      FractionBox["3", "13"]},
     {
      TagBox["\<\"HHT\"\>",
       HoldForm], 
      FractionBox["6811", "7270"], 
      FractionBox["637", "790"], 
      FractionBox["847", "1000"], 
      FractionBox["49", "100"], 
      FractionBox["70", "79"], 
      FractionBox["10", "13"], "Null", 
      FractionBox["3", "10"], 
      FractionBox["3", "10"]},
     {
      TagBox["\<\"HHH\"\>",
       HoldForm], 
      FractionBox["47677", "53590"], 
      FractionBox["4459", "6430"], 
      FractionBox["5929", "7900"], 
      FractionBox["343", "1000"], 
      FractionBox["490", "643"], 
      FractionBox["70", "121"], 
      FractionBox["7", "10"], "Null", 
      FractionBox["343", "1000"]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxDividers->{
     "Columns" -> {False, True, {False}, False}, "ColumnsIndexed" -> {}, 
      "Rows" -> {False, True, {False}, False}, "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   {OutputFormsDump`HeadedRows, OutputFormsDump`HeadedColumns}],
  Function[BoxForm`e$, 
   TableForm[
   BoxForm`e$, 
    TableHeadings -> {{
      "TTT", "TTH", "THT", "THH", "HTT", "HTH", "HHT", "HHH", "Min"}, {
      "TTT", "TTH", "THT", "THH", "HTT", "HTH", "HHT", "HHH", 
       "Min"}}]]]], "Output",
 CellChangeTimes->{
  3.50440288615349*^9, 3.5044029519382524`*^9, 3.504417365525118*^9, {
   3.5044228235432987`*^9, 3.5044228431344194`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"\<\"T\"\>", ",", "\<\"H\"\>", ",", "\<\"H\"\>"}], "}"}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.50440288615349*^9, 3.5044029519382524`*^9, 3.504417365525118*^9, {
   3.5044228235432987`*^9, 3.504422843192423*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"patterns", "[", 
  RowBox[{"[", 
   RowBox[{"Flatten", "[", 
    RowBox[{"Position", "[", 
     RowBox[{
      RowBox[{"ProbMatrix", "[", 
       RowBox[{"[", 
        RowBox[{"All", ",", "9"}], "]"}], "]"}], ",", 
      RowBox[{"Max", "[", 
       RowBox[{"ProbMatrix", "[", 
        RowBox[{"[", 
         RowBox[{"All", ",", "9"}], "]"}], "]"}], "]"}]}], "]"}], "]"}], 
   "]"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"\<\"T\"\>", ",", "\<\"H\"\>", ",", "\<\"H\"\>"}], "}"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.504421518583659*^9, 3.5044230104719906`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\"\<T\>\"", ",", "\"\<H\>\"", ",", "\"\<H\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<H\>\"", ",", "\"\<H\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<T\>\"", ",", "\"\<T\>\"", ",", "\"\<T\>\""}], "}"}]}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\"\<H\>\"", ",", "\"\<T\>\""}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"7", "/", "10"}], ",", 
     RowBox[{"3", "/", "10"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5044232077172723`*^9, 3.504423238003004*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["30441", "7270"], ",", 
   FractionBox["418509", "727000"], ",", 
   FractionBox["343", "1000"], ",", 
   FractionBox["5913", "72700"]}], "}"}]], "Output",
 CellChangeTimes->{3.504423239739104*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", "%", "]"}]], "Input",
 CellChangeTimes->{{3.5044232421522417`*^9, 3.5044232459944615`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "4.187207702888583`", ",", "0.575665749656121`", ",", "0.343`", ",", 
   "0.08133425034387895`"}], "}"}]], "Output",
 CellChangeTimes->{3.5044232470235205`*^9}]
}, Open  ]]
},
WindowSize->{1264, 633},
WindowMargins->{{-9, Automatic}, {Automatic, -8}},
ShowSelection->True,
Magnification:>FEPrivate`If[
  FEPrivate`Equal[FEPrivate`$VersionNumber, 6.], 1.25, 1.25 Inherited],
FrontEndVersion->"8.0 for Linux x86 (64-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1257, 32, 4126, 109, 382, "Input"],
Cell[5386, 143, 4184, 136, 267, "Output"],
Cell[9573, 281, 276, 7, 36, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9886, 293, 423, 13, 36, "Input"],
Cell[10312, 308, 200, 5, 36, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10549, 318, 677, 18, 60, "Input"],
Cell[11229, 338, 252, 7, 54, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11518, 350, 124, 2, 36, "Input"],
Cell[11645, 354, 202, 5, 36, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature Yx0KN1hsDiECNAwqQ9lRFVxM *)
