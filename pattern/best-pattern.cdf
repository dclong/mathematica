(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 8.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc.                                               *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       835,         17]
NotebookDataLength[     10121,        322]
NotebookOptionsPosition[     10324,        312]
NotebookOutlinePosition[     10782,        330]
CellTagsIndexPosition[     10739,        327]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"patterns", "=", 
   RowBox[{"Tuples", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "1"}], "}"}], ",", "3"}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ProbMatrix", "=", 
   RowBox[{"Table", "[", 
    RowBox[{"Null", ",", 
     RowBox[{"{", "8", "}"}], ",", 
     RowBox[{"{", "8", "}"}]}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"For", "[", 
   RowBox[{
    RowBox[{"i", "=", "1"}], ",", 
    RowBox[{"i", "<", "8"}], ",", 
    RowBox[{"i", "++"}], ",", "\[IndentingNewLine]", 
    RowBox[{"For", "[", 
     RowBox[{
      RowBox[{"j", "=", 
       RowBox[{"i", "+", "1"}]}], ",", 
      RowBox[{"j", "\[LessEqual]", "8"}], ",", 
      RowBox[{"j", "++"}], ",", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"probs", "=", 
        RowBox[{"PatternFirstComeOutProbability", "[", 
         RowBox[{
          RowBox[{"{", 
           RowBox[{
            RowBox[{"patterns", "[", 
             RowBox[{"[", "i", "]"}], "]"}], ",", 
            RowBox[{"patterns", "[", 
             RowBox[{"[", "j", "]"}], "]"}]}], "}"}], ",", 
          RowBox[{"{", 
           RowBox[{"1", ",", "0"}], "}"}], ",", 
          RowBox[{"{", 
           RowBox[{
            RowBox[{"7", "/", "10"}], ",", 
            RowBox[{"3", "/", "10"}]}], "}"}]}], "]"}]}], ";", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"ProbMatrix", "[", 
         RowBox[{"[", 
          RowBox[{"i", ",", "j"}], "]"}], "]"}], "=", 
        RowBox[{"probs", "[", 
         RowBox[{"[", "2", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"ProbMatrix", "[", 
         RowBox[{"[", 
          RowBox[{"j", ",", "i"}], "]"}], "]"}], "=", 
        RowBox[{"probs", "[", 
         RowBox[{"[", "3", "]"}], "]"}]}], ";"}]}], "\[IndentingNewLine]", 
     "]"}]}], "\[IndentingNewLine]", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"For", "[", 
   RowBox[{
    RowBox[{"i", "=", "1"}], ",", 
    RowBox[{"i", "\[LessEqual]", "8"}], ",", 
    RowBox[{"i", "++"}], ",", "\[IndentingNewLine]", 
    RowBox[{"AppendTo", "[", 
     RowBox[{
      RowBox[{"ProbMatrix", "[", 
       RowBox[{"[", "i", "]"}], "]"}], ",", 
      RowBox[{"Min", "[", 
       RowBox[{"Drop", "[", 
        RowBox[{
         RowBox[{"ProbMatrix", "[", 
          RowBox[{"[", "i", "]"}], "]"}], ",", 
         RowBox[{"Flatten", "[", 
          RowBox[{"Position", "[", 
           RowBox[{
            RowBox[{"ProbMatrix", "[", 
             RowBox[{"[", "i", "]"}], "]"}], ",", "Null"}], "]"}], "]"}]}], 
        "]"}], "]"}]}], "]"}]}], "\[IndentingNewLine]", "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"TableForm", "[", 
  RowBox[{"ProbMatrix", ",", 
   RowBox[{"TableHeadings", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"AppendTo", "[", 
       RowBox[{"patterns", ",", "\"\<Min\>\""}], "]"}], ",", "patterns"}], 
     "}"}]}]}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"patterns", "[", 
  RowBox[{"[", 
   RowBox[{"Flatten", "[", 
    RowBox[{"Position", "[", 
     RowBox[{
      RowBox[{"ProbMatrix", "[", 
       RowBox[{"[", 
        RowBox[{"All", ",", "9"}], "]"}], "]"}], ",", 
      RowBox[{"Max", "[", 
       RowBox[{"ProbMatrix", "[", 
        RowBox[{"[", 
         RowBox[{"All", ",", "9"}], "]"}], "]"}], "]"}]}], "]"}], "]"}], 
   "]"}], "]"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.5042672387607327`*^9, 3.504267245032091*^9}, {
  3.5042672876045265`*^9, 3.504267289915659*^9}, {3.504402546380056*^9, 
  3.504402600911175*^9}, {3.5044026411534767`*^9, 3.5044026421325326`*^9}, {
  3.5044027219300966`*^9, 3.5044027777492895`*^9}, {3.5044028222538347`*^9, 
  3.5044028853444433`*^9}, {3.5044029420426865`*^9, 3.5044029509881983`*^9}}],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      StyleBox["\[Null]",
       ShowStringCharacters->False], 
      TagBox[
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}],
       HoldForm], 
      TagBox[
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "1"}], "}"}],
       HoldForm], 
      TagBox[
       RowBox[{"{", 
        RowBox[{"0", ",", "1", ",", "0"}], "}"}],
       HoldForm], 
      TagBox[
       RowBox[{"{", 
        RowBox[{"0", ",", "1", ",", "1"}], "}"}],
       HoldForm], 
      TagBox[
       RowBox[{"{", 
        RowBox[{"1", ",", "0", ",", "0"}], "}"}],
       HoldForm], 
      TagBox[
       RowBox[{"{", 
        RowBox[{"1", ",", "0", ",", "1"}], "}"}],
       HoldForm], 
      TagBox[
       RowBox[{"{", 
        RowBox[{"1", ",", "1", ",", "0"}], "}"}],
       HoldForm], 
      TagBox[
       RowBox[{"{", 
        RowBox[{"1", ",", "1", ",", "1"}], "}"}],
       HoldForm], 
      TagBox["\<\"Min\"\>",
       HoldForm]},
     {
      TagBox[
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "0"}], "}"}],
       HoldForm], "Null", 
      FractionBox["3", "10"], 
      FractionBox["30", "121"], 
      FractionBox["90", "727"], 
      FractionBox["27", "1000"], 
      FractionBox["1089", "7900"], 
      FractionBox["459", "7270"], 
      FractionBox["5913", "53590"], 
      FractionBox["27", "1000"]},
     {
      TagBox[
       RowBox[{"{", 
        RowBox[{"0", ",", "0", ",", "1"}], "}"}],
       HoldForm], 
      FractionBox["7", "10"], "Null", 
      FractionBox["10", "17"], 
      FractionBox["30", "79"], 
      FractionBox["9", "100"], 
      FractionBox["363", "1000"], 
      FractionBox["153", "790"], 
      FractionBox["1971", "6430"], 
      FractionBox["9", "100"]},
     {
      TagBox[
       RowBox[{"{", 
        RowBox[{"0", ",", "1", ",", "0"}], "}"}],
       HoldForm], 
      FractionBox["91", "121"], 
      FractionBox["7", "17"], "Null", 
      FractionBox["3", "10"], 
      FractionBox["79", "170"], 
      FractionBox["153", "790"], 
      FractionBox["153", "1000"], 
      FractionBox["1971", "7900"], 
      FractionBox["153", "1000"]},
     {
      TagBox[
       RowBox[{"{", 
        RowBox[{"0", ",", "1", ",", "1"}], "}"}],
       HoldForm], 
      FractionBox["637", "727"], 
      FractionBox["49", "79"], 
      FractionBox["7", "10"], "Null", 
      FractionBox["7", "10"], 
      FractionBox["51", "130"], 
      FractionBox["51", "100"], 
      FractionBox["657", "1000"], 
      FractionBox["51", "130"]},
     {
      TagBox[
       RowBox[{"{", 
        RowBox[{"1", ",", "0", ",", "0"}], "}"}],
       HoldForm], 
      FractionBox["973", "1000"], 
      FractionBox["91", "100"], 
      FractionBox["91", "170"], 
      FractionBox["3", "10"], "Null", 
      FractionBox["3", "10"], 
      FractionBox["9", "79"], 
      FractionBox["153", "643"], 
      FractionBox["9", "79"]},
     {
      TagBox[
       RowBox[{"{", 
        RowBox[{"1", ",", "0", ",", "1"}], "}"}],
       HoldForm], 
      FractionBox["6811", "7900"], 
      FractionBox["637", "1000"], 
      FractionBox["637", "790"], 
      FractionBox["79", "130"], 
      FractionBox["7", "10"], "Null", 
      FractionBox["3", "13"], 
      FractionBox["51", "121"], 
      FractionBox["3", "13"]},
     {
      TagBox[
       RowBox[{"{", 
        RowBox[{"1", ",", "1", ",", "0"}], "}"}],
       HoldForm], 
      FractionBox["6811", "7270"], 
      FractionBox["637", "790"], 
      FractionBox["847", "1000"], 
      FractionBox["49", "100"], 
      FractionBox["70", "79"], 
      FractionBox["10", "13"], "Null", 
      FractionBox["3", "10"], 
      FractionBox["3", "10"]},
     {
      TagBox[
       RowBox[{"{", 
        RowBox[{"1", ",", "1", ",", "1"}], "}"}],
       HoldForm], 
      FractionBox["47677", "53590"], 
      FractionBox["4459", "6430"], 
      FractionBox["5929", "7900"], 
      FractionBox["343", "1000"], 
      FractionBox["490", "643"], 
      FractionBox["70", "121"], 
      FractionBox["7", "10"], "Null", 
      FractionBox["343", "1000"]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxDividers->{
     "Columns" -> {False, True, {False}, False}, "ColumnsIndexed" -> {}, 
      "Rows" -> {False, True, {False}, False}, "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}],
   {OutputFormsDump`HeadedRows, OutputFormsDump`HeadedColumns}],
  Function[BoxForm`e$, 
   TableForm[
   BoxForm`e$, 
    TableHeadings -> {{{0, 0, 0}, {0, 0, 1}, {0, 1, 0}, {0, 1, 1}, {1, 0, 
       0}, {1, 0, 1}, {1, 1, 0}, {1, 1, 1}, "Min"}, {{0, 0, 0}, {0, 0, 1}, {0,
        1, 0}, {0, 1, 1}, {1, 0, 0}, {1, 0, 1}, {1, 1, 0}, {1, 1, 1}, 
       "Min"}}]]]], "Output",
 CellChangeTimes->{3.50440288615349*^9, 3.5044029519382524`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"0", ",", "1", ",", "1"}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.50440288615349*^9, 3.5044029519922557`*^9}]
}, Open  ]]
},
WindowSize->{1350, 633},
WindowMargins->{{Automatic, 5}, {Automatic, -8}},
ShowSelection->True,
Magnification:>FEPrivate`If[
  FEPrivate`Equal[FEPrivate`$VersionNumber, 6.], 1.25, 1.25 Inherited],
FrontEndVersion->"8.0 for Linux x86 (64-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1257, 32, 3790, 103, 359, "Input"],
Cell[5050, 137, 5083, 166, 267, "Output"],
Cell[10136, 305, 172, 4, 36, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature huT7uEp7OVZxGB1urRTHqVoT *)
