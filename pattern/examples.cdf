(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 8.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc.                                               *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       835,         17]
NotebookDataLength[     16206,        550]
NotebookOptionsPosition[     14941,        488]
NotebookOutlinePosition[     15377,        505]
CellTagsIndexPosition[     15334,        502]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"sp1", "=", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"pat1", "=", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0", ",", "1", ",", "0"}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"pat2", "=", 
   RowBox[{"{", 
    RowBox[{"1", ",", "1", ",", "0", ",", "0"}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"pat3", "=", 
    RowBox[{"{", 
     RowBox[{"1", ",", "1", ",", "0", ",", "0", ",", "1", ",", "1"}], "}"}]}],
    ";"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{"sample", " ", "space", " ", "2"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sp2", "=", 
   RowBox[{"{", 
    RowBox[{"0", ",", "1", ",", "2"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"prob2", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"1", "/", "2"}], ",", 
     RowBox[{"1", "/", "3"}], ",", 
     RowBox[{"1", "/", "6"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"pat4", "=", 
   RowBox[{"{", 
    RowBox[{"0", ",", "2", ",", "0"}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"pat5", "=", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0", ",", "0", ",", "2"}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"prob3", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"1", "/", 
      RowBox[{"Sqrt", "[", "2", "]"}]}], ",", 
     RowBox[{"1", "-", 
      RowBox[{"1", "/", 
       RowBox[{"Sqrt", "[", "2", "]"}]}]}]}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"patA", "=", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0", ",", "1", ",", "0"}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"patB", "=", 
   RowBox[{"{", 
    RowBox[{"0", ",", "1", ",", "0", ",", "0"}], "}"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.483620039935*^9, 3.483620318899*^9}, 
   3.483620489335*^9, {3.4836205492139997`*^9, 3.483620624275*^9}, {
   3.5037189067206287`*^9, 3.5037189437187443`*^9}, {3.503718977153657*^9, 
   3.503719093044286*^9}, {3.503770977851166*^9, 3.503771017021406*^9}, {
   3.50378033510837*^9, 3.50378034804711*^9}, {3.5037803974679365`*^9, 
   3.5037804001350894`*^9}, {3.5037810314812*^9, 3.503781063231016*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "0", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "0"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.503794824793133*^9, 3.5037948341396675`*^9}, {
  3.50426579142595*^9, 3.5042657930450425`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["9", "2"], ",", 
   FractionBox["1", "4"], ",", 
   FractionBox["1", "4"], ",", 
   FractionBox["1", "2"]}], "}"}]], "Output",
 CellChangeTimes->{3.503794838169898*^9, 3.5042657944441223`*^9, 
  3.5042658718465495`*^9, 3.5042660043601294`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<T\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<H\>\"", ",", "\"\<T\>\""}], "}"}]}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\"\<H\>\"", ",", "\"\<T\>\""}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.504424192685609*^9, 3.5044241962028103`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"4", ",", 
   FractionBox["1", "2"], ",", 
   FractionBox["1", "2"]}], "}"}]], "Output",
 CellChangeTimes->{3.5044241987699575`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<T\>\"", ",", "\"\<T\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<T\>\"", ",", "\"\<H\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<H\>\"", ",", "\"\<T\>\""}], "}"}]}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\"\<H\>\"", ",", "\"\<T\>\""}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.504423977062276*^9, 3.504424023211916*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["9", "2"], ",", 
   FractionBox["1", "4"], ",", 
   FractionBox["1", "4"], ",", 
   FractionBox["1", "2"]}], "}"}]], "Output",
 CellChangeTimes->{3.5044240269851313`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"patA", ",", "patB"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5037812083863187`*^9, 3.5037812223991203`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["90", "7"], ",", 
   FractionBox["9", "14"], ",", 
   FractionBox["5", "14"]}], "}"}]], "Output",
 CellChangeTimes->{3.5037812231251616`*^9, 3.503790021921424*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"pat1", ",", "pat2"}], "}"}], ",", "sp1"}], "]"}]], "Input",
 CellChangeTimes->{{3.504266095623349*^9, 3.5042661387838173`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["80", "9"], ",", 
   FractionBox["4", "9"], ",", 
   FractionBox["5", "9"]}], "}"}]], "Output",
 CellChangeTimes->{3.5042661399878864`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "0", ",", "1", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "0", ",", "0"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5042661667724185`*^9, 3.5042661890216913`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["80", "9"], ",", 
   FractionBox["4", "9"], ",", 
   FractionBox["5", "9"]}], "}"}]], "Output",
 CellChangeTimes->{{3.5042661824093127`*^9, 3.504266190822794*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "1", ",", "1", ",", "1"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5042665508853884`*^9, 3.504266592406763*^9}, {
  3.504424404686735*^9, 3.504424494396866*^9}, {3.5044245943255816`*^9, 
  3.5044246257903814`*^9}, {3.5044247017337255`*^9, 3.504424751261558*^9}, {
  3.5044248537104177`*^9, 3.5044248545174637`*^9}, {3.504424896966892*^9, 
  3.5044248971179004`*^9}, {3.5044249699430656`*^9, 3.5044249847819147`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"14", ",", 
   FractionBox["1", "8"], ",", 
   FractionBox["7", "8"]}], "}"}]], "Output",
 CellChangeTimes->{{3.5042665814601374`*^9, 3.504266593078802*^9}, {
   3.5044244077229085`*^9, 3.504424467437324*^9}, 3.504424497468042*^9, {
   3.504424597739777*^9, 3.50442462645642*^9}, {3.504424715664522*^9, 
   3.5044247524966288`*^9}, 3.504424856814595*^9, 3.504424898898002*^9, 
   3.5044249894321804`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "1", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "1", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "1", ",", "1", ",", "1"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5044250025499306`*^9, 3.504425011831462*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["113", "8"], ",", 
   FractionBox["1", "16"], ",", 
   FractionBox["1", "16"], ",", 
   FractionBox["7", "8"]}], "}"}]], "Output",
 CellChangeTimes->{3.5044250133595495`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "1", ",", "0", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", "}"}], ",", 
     RowBox[{
      RowBox[{"{", "}"}], 
      RowBox[{"{", 
       RowBox[{"1", ",", "1", ",", "0", ",", "1", ",", "1"}], "}"}]}]}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5044248713264256`*^9, 3.5044249148129125`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["97", "12"], ",", 
   FractionBox["7", "24"], ",", 
   FractionBox["7", "24"], ",", 
   FractionBox["5", "12"]}], "}"}]], "Output",
 CellChangeTimes->{3.5044248800889263`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "0", ",", "1"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["9", "2"], ",", 
   FractionBox["3", "4"], ",", 
   FractionBox["1", "4"]}], "}"}]], "Output",
 CellChangeTimes->{3.5044251079019566`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"PatternFirstComeOutProbability", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"\"\<H\>\"", ",", "\"\<H\>\""}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"\"\<H\>\"", ",", "\"\<T\>\"", ",", "\"\<H\>\""}], "}"}]}], 
     "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\"\<H\>\"", ",", "\"\<T\>\""}], "}"}]}], "]"}], 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.5044251228168097`*^9, 3.5044251386727166`*^9}, {
  3.50442567844059*^9, 3.504425685649002*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["14", "3"], ",", 
   FractionBox["2", "3"], ",", 
   FractionBox["1", "3"]}], "}"}]], "Output",
 CellChangeTimes->{3.504425139811782*^9, 3.504425687213091*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<H\>\"", ",", "\"\<T\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<H\>\"", ",", "\"\<H\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<T\>\"", ",", "\"\<H\>\""}], "}"}]}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\"\<H\>\"", ",", "\"\<T\>\""}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5044256984077315`*^9, 3.504425710008395*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["16", "3"], ",", 
   FractionBox["1", "3"], ",", 
   FractionBox["1", "3"], ",", 
   FractionBox["1", "3"]}], "}"}]], "Output",
 CellChangeTimes->{3.5044257124825363`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "0", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "0", ",", "1", ",", "1"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.504424612093598*^9, 3.5044246329897933`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["33", "4"], ",", 
   FractionBox["3", "8"], ",", 
   FractionBox["3", "8"], ",", 
   FractionBox["1", "4"]}], "}"}]], "Output",
 CellChangeTimes->{{3.5044246212261205`*^9, 3.5044246336128287`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "0", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "1", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "1", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "0", ",", "1", ",", "1"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "0"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5044244233108006`*^9, 3.504424473093648*^9}, {
  3.504424503438383*^9, 3.5044245277677746`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["15", "2"], ",", 
   FractionBox["3", "16"], ",", 
   FractionBox["3", "16"], ",", 
   FractionBox["3", "16"], ",", 
   FractionBox["3", "16"], ",", 
   FractionBox["1", "4"]}], "}"}]], "Output",
 CellChangeTimes->{3.50442447646284*^9, 3.5044245310889645`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"PatternFirstComeOutProbability", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<T\>\""}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"\"\<H\>\"", ",", "\"\<H\>\"", ",", "\"\<T\>\""}], "}"}]}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\"\<H\>\"", ",", "\"\<T\>\""}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.5044243900879*^9, 3.504424390871945*^9}}]
},
WindowSize->{1192, 633},
WindowMargins->{{-9, Automatic}, {Automatic, -8}},
Magnification:>FEPrivate`If[
  FEPrivate`Equal[FEPrivate`$VersionNumber, 6.], 1.5, 1.5 Inherited],
FrontEndVersion->"8.0 for Linux x86 (64-bit) (October 10, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[1235, 30, 2278, 68, 341, "Input"],
Cell[CellGroupData[{
Cell[3538, 102, 521, 14, 43, "Input"],
Cell[4062, 118, 298, 8, 68, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4397, 131, 449, 12, 43, "Input"],
Cell[4849, 145, 171, 5, 68, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5057, 155, 562, 14, 71, "Input"],
Cell[5622, 171, 225, 7, 68, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5884, 183, 339, 9, 43, "Input"],
Cell[6226, 194, 218, 6, 68, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6481, 205, 231, 5, 43, "Input"],
Cell[6715, 212, 194, 6, 93, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6946, 223, 420, 11, 64, "Input"],
Cell[7369, 236, 218, 6, 93, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7624, 247, 712, 15, 64, "Input"],
Cell[8339, 264, 443, 9, 93, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8819, 278, 501, 13, 64, "Input"],
Cell[9323, 293, 229, 7, 93, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9589, 305, 594, 17, 106, "Input"],
Cell[10186, 324, 230, 7, 93, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10453, 336, 330, 10, 64, "Input"],
Cell[10786, 348, 193, 6, 93, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11016, 359, 548, 15, 106, "Input"],
Cell[11567, 376, 214, 6, 93, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11818, 387, 564, 14, 106, "Input"],
Cell[12385, 403, 226, 7, 93, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12648, 415, 501, 13, 64, "Input"],
Cell[13152, 430, 252, 7, 93, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13441, 442, 718, 18, 106, "Input"],
Cell[14162, 462, 315, 9, 93, "Output"]
}, Open  ]],
Cell[14492, 474, 445, 12, 64, "Input"]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature JxpUr7Q7fo6iLA1MBV1QtS3q *)
